package org.vanis.camel.errorhandling;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

public class IrecovarebleCaseRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("timer://foo?fixedRate=true&period=1000")
                .log(LoggingLevel.INFO, "Route started")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        // irecoverable error stop processing
                        exchange.getOut().setFault(true);
                    }
                })
                // We should not see this log
                .log(LoggingLevel.INFO, "Recieved ${body}");


    }
}
