package org.vanis.camel.errorhandling;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

public class RecoverableCaseRouteBuilder  extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("timer://foo?fixedRate=true&period=1000")
                .log(LoggingLevel.INFO, "Route started")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        // ecoverable error stop processing
                        throw new RuntimeException("My runtime exception");
                    }
                })
                        // We should not see this log
                .log(LoggingLevel.INFO, "Recieved ${body}");


    }
}
