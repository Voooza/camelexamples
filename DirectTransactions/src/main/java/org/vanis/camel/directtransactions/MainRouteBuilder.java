package org.vanis.camel.directtransactions;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.jms.JmsComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vanis.camel.directtransactions.dao.TaskDAO;
import org.vanis.camel.directtransactions.entities.Task;
import org.vanis.camel.directtransactions.transaction.CdiTransactionManager;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;

/**
 * Lets build a route
 * Created by voooza on 4/24/15.
 */
@Startup
@ApplicationScoped
@ContextName("jms-camel-context")
public class MainRouteBuilder extends RouteBuilder {

    private static final Logger log = LoggerFactory.getLogger(MainRouteBuilder.class);

    @Resource(mappedName = "java:/JmsXA")
    private ConnectionFactory connectionFactory;

    @Inject
    CdiTransactionManager transactionManager;

    @Inject
    TaskDAO taskDAO;

    private static final String endpoint = "seda:a";

    @Override
    public void configure() throws Exception {
        // @formatter:off
        JmsComponent jmsComponent = JmsComponent.jmsComponentTransacted(connectionFactory, transactionManager);
        getContext().addComponent("jms", jmsComponent);


        from("timer://foo?fixedRate=true&period=100000")
                .transacted("PROPAGATION_REQUIRED")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        Task t = new Task();
                        t.setTaskType("integration");
                        t.setParentTaskId(0);
                        t.setDependentTaskId(12);
                        t.setNote("created");
                        taskDAO.saveTask(t);
                        log.info(t.toString());
                        Task newTask = taskDAO.getTask(t.getTaskId());
                        exchange.getIn().setBody(newTask.getTaskId());
                    }
                })
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        Task newTask = taskDAO.getTask((Integer)exchange.getIn().getBody());
                        newTask.setNote("will save now");
                        log.info(newTask.toString());
                    }
                })
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        Task newTask = taskDAO.getTask((Integer)exchange.getIn().getBody());
                        newTask.setNote("will update");
                        log.info(newTask.toString());
                    }
                })
                .to(endpoint);

                from(endpoint)
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        throw new Exception("Just for fun :-)");
                    }
                })
                .log(LoggingLevel.INFO, "Timer ticked.");
        // @formatter:on
    }

    @PostConstruct
    public void init() {
        try {
            getContext().start();
            log.info("Camel context started");
        } catch (Exception e) {
            log.error("Failed to start camel context", e);
        }
    }

    @PreDestroy
    public void shutdown() {
        try {
            getContext().stop();
            log.info("CamelContext stopped .");
        } catch (Exception e) {
            log.error("Failed to stop camel context", e);
        }
    }
}
