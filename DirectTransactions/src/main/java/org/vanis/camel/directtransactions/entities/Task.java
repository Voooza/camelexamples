package org.vanis.camel.directtransactions.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Task {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int taskId;
    private String taskType;
    private int parentTaskId;
    private int dependentTaskId;
    private String note;

    public Task() {
    }

    public Task(int taskId, String taskType, int parentTaskId, int dependentTaskId, String note) {
        this.taskId = taskId;
        this.taskType = taskType;
        this.parentTaskId = parentTaskId;
        this.dependentTaskId = dependentTaskId;
        this.note = note;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public int getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(int parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public int getDependentTaskId() {
        return dependentTaskId;
    }

    public void setDependentTaskId(int dependentTaskId) {
        this.dependentTaskId = dependentTaskId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder("\nTask:{\n");
        sb.append("\ttaskId          : ").append(taskId).append(",\n");
        sb.append("\ttaskType        : ").append(taskType).append(",\n");
        sb.append("\tparentTaskId    : ").append(parentTaskId).append(",\n");
        sb.append("\tdependentTaskId : ").append(dependentTaskId).append("\n");
        sb.append("\tnote            : ").append(note).append("\n");
        sb.append("};");
        return sb.toString();
    }
}
