package org.vanis.camel.directtransactions.transaction;

import org.springframework.transaction.jta.JtaTransactionManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;

@Named("transactionManager")
public class CdiTransactionManager extends JtaTransactionManager {

    @Resource(mappedName = "java:/TransactionManager")
    private TransactionManager transactionManager;

    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void initTransactionManager() {
        setTransactionManager(transactionManager);
        setUserTransaction(userTransaction);
    }
}